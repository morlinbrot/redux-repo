import { createRepo } from './createRepo';

describe('createRepo', () => {
  it('has a test stub', () => {
    const repo = createRepo({
      name: 'test',
    });

    expect(repo).toHaveProperty('adapter');
    expect(repo).toHaveProperty('fetch');
    expect(repo).toHaveProperty('slice');
  });
})