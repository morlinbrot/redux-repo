import { EntityId } from '.';

export function isEntityId(test: unknown): test is EntityId {
  return (
    typeof test === 'number'
    || typeof test === 'string'
  );
}

export function isEntityIdArray(test: unknown): test is EntityId[] {
  return (
    Array.isArray(test)
    && test.every(item => isEntityId(item))
  )
}