import {
  createEntityAdapter,
  createSlice,
  Dispatch,
  Update,
} from '@reduxjs/toolkit';

import {
  isEntityId,
  isEntityIdArray
} from './typeGuards';

import reduxRepo, {
  EntityId,
  EntityState,
  ReduceOperation,
  RepoFetchOpts,
  RepoOpts,
} from '.';


const log = {
  err: (msg: string) => {
    console.error('[redux-repo] Error: ', msg);
  }
}


/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
export function createRepo<Entity extends any>({
  name,
  selectId,
  extraReducers,
  sortComparer,
}: RepoOpts<Entity>) {

  const adapter = createEntityAdapter({
    sortComparer,
    selectId: selectId ? selectId : (entity: Entity) => entity.Guid,
  });

  const initEntityState: EntityState<Entity> = {
    entities: {},
    ids: [],
    loading: 'idle',
  }

  const slice = createSlice({
    extraReducers,
    name,
    initialState: adapter.getInitialState(initEntityState),
    reducers: {
      addMany: adapter.addMany,
      addOne: adapter.addOne,
      removeAll: adapter.removeAll,
      removeMany: adapter.removeMany,
      removeOne: adapter.removeOne,
      setAll: adapter.setAll,
      updateMany: adapter.updateMany,
      updateOne: adapter.updateOne,
      upsertMany: adapter.upsertMany,
      upsertOne: adapter.upsertOne,
      setIdle: state => {
        state.loading = 'idle';
      },
      setPending: state => {
        state.loading = 'pending';
      },
    },
  });

  const fetch = <Result extends EntityId | EntityId[] | Entity | Entity[]>({
    fetchFn,
    fetchFnArgs = [],
    operation,
    processor,
  }: RepoFetchOpts<Result>) => {
    return async (dispatch: Dispatch, getState: () => EntityState<Entity>) => {

      try {
        let res: Result;

        dispatch(slice.actions.setPending());
        const fetchRes = await fetchFn(...fetchFnArgs);

        res = reduxRepo.defaultProcessor(fetchRes);

        if (processor) {
          res = processor(res, getState());
        }

        switch(operation) {
          case ReduceOperation.AddMany: {
            if (!Array.isArray(res)) { log.err('Result must be of type Entity[].'); return; }
            dispatch(slice.actions.addMany(res));
            break;
          }
          case ReduceOperation.AddOne: {
            dispatch(slice.actions.addOne(res));
            break;
          }
          case ReduceOperation.RemoveAll: {
            if (typeof res === 'boolean') { log.err('Result must be of type boolean.'); return; }
            if (!res) { log.err('Result must be true.'); return; }
            dispatch(slice.actions.removeAll());
            break;
          }
          case ReduceOperation.RemoveMany: {
            if (!isEntityIdArray(res)) { log.err('Result must be of type EntityId[].'); return; }
            dispatch(slice.actions.removeMany(res));
            break;
          }
          case ReduceOperation.RemoveOne: {
            if (!isEntityId(res)) { log.err('Result must be of type EntityId.'); return; }
            dispatch(slice.actions.removeOne(res));
            break;
          }
          case ReduceOperation.SetAll: {
            if (!Array.isArray(res)) { log.err('Result must be of type Entity[].'); return; }
            dispatch(slice.actions.setAll(res));
            break;
          }
          case ReduceOperation.UpdateMany: {
            if (!Array.isArray(res)) { log.err('Result must be of type Update<EntityId>[].'); return; }
            dispatch(slice.actions.updateMany(res));
            break;
          }
          case ReduceOperation.UpdateOne: {
            dispatch(slice.actions.updateOne(res as unknown as Update<Entity>));
            break;
          }
          case ReduceOperation.UpsertMany: {
            if (!Array.isArray(res)) { log.err('Result must be of type Entity[].'); return; }
            dispatch(slice.actions.upsertMany(res));
            break;
          }
          case ReduceOperation.UpsertOne: {
            dispatch(slice.actions.upsertOne(res));
            break;
          }
        }

        return res;

      } catch (err) {

        reduxRepo.errorHandler(err);
        dispatch(slice.actions.setIdle());
        return err;

      }
    }
  }

  return {
    adapter,
    fetch,
    slice,
  }
}
