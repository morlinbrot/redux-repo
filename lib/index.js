export * from './createRepo';
export var ReduceOperation;
(function (ReduceOperation) {
    ReduceOperation["AddMany"] = "AddMany";
    ReduceOperation["AddOne"] = "AddOne";
    ReduceOperation["RemoveAll"] = "RemoveAll";
    ReduceOperation["RemoveMany"] = "RemoveMany";
    ReduceOperation["RemoveOne"] = "RemoveOne";
    ReduceOperation["SetAll"] = "SetAll";
    ReduceOperation["UpdateMany"] = "UpdateMany";
    ReduceOperation["UpdateOne"] = "UpdateOne";
    ReduceOperation["UpsertMany"] = "UpsertMany";
    ReduceOperation["UpsertOne"] = "UpsertOne";
})(ReduceOperation || (ReduceOperation = {}));
class ReduxRepo {
    constructor() {
        this.defaultProcessor = (res) => res;
        this.errorHandler = (err) => { console.error('[redux-repo] Error: ', err); };
    }
    init({ defaultProcessor, errorHandler, }) {
        if (defaultProcessor) {
            this.defaultProcessor = defaultProcessor;
        }
        if (errorHandler) {
            this.errorHandler = errorHandler;
        }
    }
}
const reduxRepo = new ReduxRepo();
export default reduxRepo;
