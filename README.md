# Redux Repo
A thin convenience abstraction over @reduxjs/toolkit trying to make day-to-day Redux tasks as simple as possible.

---
# Usage
To be expanded at a later time.
## Update store with backend data

```typescript
import { createRepo, ReduceOperation } from 'redux-repo'

// We're building an app that displays a selection of awesome quotes.
const quotes = createRepo<Quote>({
  name: 'quotes',
  selectId: quote => quote.id,
});

export const fetchQuotes = (...args: any) => {
  return quotesRepo.fetch({
    fetchFn: api.fetchQuotes,
    fetchFnArgs: [ ...args ],
    operation: ReduceOperation.AddMany,
  });
};

export const actions = quotesRepo.slice.actions;
export const quotes = quotesRepo.slice.reducer;
export const selectors = quotesRepo.adapter.getSelectors((state: RootAppState) => state.quotes);
export const quotesSelector = selectors.selectAll;
```

## Update multiple parts of the store with a single thunk

## Non-async store for "local" app state

## Intercepting fetch response with `processor`

## Configuring Redux Repo